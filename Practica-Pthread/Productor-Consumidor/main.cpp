#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
 #include <unistd.h>

using namespace std;

int tam = 100;
int * vec = new int [tam];

pthread_t thread1, thread2;
pthread_mutex_t prod;
pthread_mutex_t cons;

//para productor consumidor  creo un while(1) para q no acabe nunca y produsca y consuma
void * producir(void * var){

	srand (time(NULL));
	while(1){

		for(int i=0;i<tam;i++){
			//primero bloquelo parar q si el consumidor lo alcance no tome la misma posicion
			pthread_mutex_lock(&prod);
			if(vec[i] == (-1)){
				int num = rand() % 10;
				vec[i] = num;
				cout <<"insertardo : "<<num<<endl;
				usleep( num * 100000 );	
			}
			if(i == tam -1){
				i = 0;
			}
			//una vez que terminar libero el mutex
			pthread_mutex_unlock(&prod);

		}
	}
	
}


void * consumir(void * var)
{
	while(1){

		for(int i =0; i < tam;i++){
			//bloqueo para que si el productor lo alcanza no tome la misma posicion
			pthread_mutex_lock(&cons);

			if(vec[i] != -1){
				int aux = vec[i];
				vec[i] = (-1);
				cout <<"eliminado  : "<<aux<<endl;
				usleep(aux * 100000 );	
			}
			if(i == tam -1){
				i = 0;
			}
			
			pthread_mutex_unlock(&cons);
		}

	}
}

int main()
{
	srand (time(NULL));

	for(int i = 0; i< tam ; i++){
		int num = rand() % 10;
		vec[i] = num;
	}

	pthread_create(&thread1,NULL,producir,(void *)1);
	pthread_create(&thread2,NULL,consumir,(void *)1);
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);

	return 0;
}