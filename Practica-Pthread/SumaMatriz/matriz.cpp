#include <stdio.h>
#include <pthread.h>
#include <iostream>

#define NTHREADS 10

using namespace std;

pthread_mutex_t mutex1;

int cont = 0;
int res =0;

int matriz [NTHREADS][NTHREADS];


void llenar()
{
	for(int i=0;i<NTHREADS;i++)
	{
		for(int j=0; j<NTHREADS;j++)
		{
			matriz[i][j]= i+j;
		}
	}
}

void *suma(void * n)
{
	

	for(int i=0;i<NTHREADS; i++)
	{
		res += matriz[cont][i];
	}
	
	pthread_mutex_lock( &mutex1 );
	cont++;
	pthread_mutex_unlock( &mutex1 );

	cout << "respuesta : "<<res<<endl;

	
}


int main()
{
	pthread_t thread[NTHREADS];
	llenar();
	for(int i=0; i < NTHREADS; i++){

		pthread_create( &thread[i], NULL, &suma, (void *)1);
	}
	
	for(int j=0; j < NTHREADS; j++){
		pthread_join( thread[j], NULL);
	}
	
	cout <<"total : "<<res<<endl;
}

