#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
 #include <unistd.h>

using namespace std;

pthread_t thread1, thread2;
pthread_mutex_t mutex1;
pthread_mutex_t mutex2;

int tam = 100;
int a[100][100];
int b[100][100];
int c[100][100];
int d[100][100];
int ab[100][100];
int cd[100][100];
int res[100][100];

void * iniAB(void * data){

	for(int i=0; i<100; i++){
		for(int j=0; j<100;j++){
			a[i][j] = i+j;
			b[i][j] = i+2*j;
			ab[i][j] = 0;
			res[i][j] = 0;
		}
	}

}

void * iniCD(void * data){

	for(int i=0; i<100; i++){
		for(int j=0; j<100;j++){
			c[i][j] = (2*i) + (3*j);
			d[i][j] = (2*i)+j;
			cd[i][j] = 0;

		}
	}
}

void * multAb(void * data){

	for(int i=0;i<tam;i++){
		for(int j=0;j<tam;j++){
			for(int k=0;k<tam;k++){
				ab[i][j]+= a[i][k]* b[k][j];
			}
		}	
	}
}

void * multcd(void * data){

	for(int i=0;i<tam;i++){
		for(int j=0;j<tam;j++){
			for(int k=0;k<tam;k++){
				cd[i][j]+= c[i][k] * d[k][j];
			}
		}	
	}
}


int main(){

/******INICIALIZANDO LAS MATRICES **************/
	pthread_create(&thread1,NULL,iniAB,(void *)1);
	pthread_create(&thread2,NULL,iniCD,(void *)1);
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);
/******FIN **************/

/******MULTIPLICANDO LAS MATRICES **************/

	pthread_create(&thread1,NULL,multAb,(void *)1);
	pthread_create(&thread2,NULL,multcd,(void *)1);
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);

/******FIN **************/

	for (int i = 0; i < tam; i++){
		for (int j = 0; j < tam; j++){
			res[i][j] = ab[i][j] + cd[i][j];
		}
	}

	for (int i = 0; i < tam; ++i){
		for (int j = 0; j < tam; ++j){
			cout << res[i][j]<<" ";
		}
		cout <<endl;
	}
	return 0;
}
