#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {

        int rank, proc;
        int cont = 0;
        int tag = 5;
        int i,j;
        MPI_Status status;

        const int root=0;

        MPI_Init(&argc, &argv);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &proc);

        while(cont < 21){
            if(rank == root){
                for(i =0 ; i<proc;i++){
                    MPI_Send(&cont,1,MPI_INT,i,i,MPI_COMM_WORLD);    
                }
                for(j =0; j<proc ;j++){
                    MPI_Recv(&cont,1,MPI_INT,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
                    /*MPI_Send(&cont,1,MPI_INT,1,tag,MPI_COMM_WORLD);*/
                    printf("Proc. %d contador %d.\n", rank,status.MPI_SOURCE);
                    cont++;
                    
                }
            }
            else{
                MPI_Recv(&cont,1,MPI_INT,root,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
                printf("Proc. %d contador %d.\n", rank,status.MPI_SOURCE);
                cont++;
                MPI_Send(&cont,1,MPI_INT,root,tag,MPI_COMM_WORLD);
            }
        }

        MPI_Finalize();
        return 0;
}