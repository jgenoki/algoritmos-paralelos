#include <stdio.h>
#include <mpi.h>

void mybcast(void * dato, int contador, MPI_Datatype tipoDato, int root, MPI_Comm comunicador){
	
	int rank;
	int tam;

	MPI_Comm_rank(comunicador,&rank);
	MPI_Comm_size(comunicador,&tam);

	if(rank == root){
		int i;
		for( i=0; i<tam;i++){
			if(i != rank){
				MPI_Send(dato,contador,tipoDato,i,0,comunicador);
					/*printf("[%d]: Antes de broadCast, el buffer %d\n", rank, dato);
*/
			}

		}
	}
	else{
		MPI_Recv(dato,contador,tipoDato,root,0,comunicador,MPI_STATUS_IGNORE);
			

	}
}

int main(int argc, char** argv)
{
	
	int rank , tam;
	int cont;
	

	MPI_Init(&argc, &argv);// inicio mpi
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &tam);
	
	if(rank == 0){
		int buffer = 50;
		mybcast(&buffer, 1, MPI_INT , 0, MPI_COMM_WORLD);
		printf("[%d]: Despues de broadCast, el buffer %d\n", rank, buffer);
	}
	
	return 0;
}