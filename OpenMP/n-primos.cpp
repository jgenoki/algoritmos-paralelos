#include <omp.h>
#include <iostream>
#include <stdio.h>

using namespace std;

int main(){

	bool primo;
	int i,j;
	int num=1000000;
	int cont=0;

	#pragma omp parallel for schedule(dynamic,100) num_threads(8) reduction(+:cont)
	for(i = 0;i <= num;i++){
		primo = 1;
		for(j=2; j<i;j++){
			if(i % j ==0){
				primo = 0;
				break;	
			}
		}
		if(primo){
			cont+=i;
		}
	}

	cout <<"suma de primos : "<<cont<<endl;
	return 0;
}