#include <omp.h>
#include <iostream>

using namespace std;

int main(){

	const int tam = 5;
	int mat_uno[tam][tam];
	int mat_dos[tam][tam];
	int mat_res[tam][tam];

	for(int i=0; i<tam; i++){
		for(int j=0; j<tam; j++){
			mat_uno[i][j] = 1;
			mat_dos[i][j] = 2;
			mat_res[i][j] = 0;
		}
	}
	int i,j,k;
	int cont = 0;
	#pragma omp parallel for schedule (static) private (i, j, k) 
	/*#pragma omp parallel for num_threads(4)\	
	reduction(+: cont)*/
		for(i=0; i<tam; i++){
			for(j=0; j<tam; j++){
				for(k=0; k<tam; k++){
					mat_res[i][j] += mat_uno[i][k] * mat_dos[k][j];
					/*cont += mat_uno[i][k] * mat_dos[k][j];
					mat_res[i][j] = cont;*/
				}
			}
		}	
	
	

	for(int i=0; i<tam; i++){
		for(int j=0; j<tam; j++){
			cout <<mat_res[i][j]<<" ";
		}
		cout<<endl;
	}
	return 0;
}