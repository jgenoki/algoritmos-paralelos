#include <stdio.h>
#include <mpi.h>
#include <omp.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define tam 10

int mat[tam][tam];
int vec_master[tam];

void sumar(int n, int *v)
{
	int i;
	for (i = 0; i < tam; ++i)
	{
		v[i] = v[i]+n;
	}
}

void  print(int *v)
{
	int i;
	for (i = 0; i < tam; ++i)
	{
		cout<< v[i] << " ";
	}
	cout<<endl;
}


int main(int argc, char ** argv)
{
	int i, j;
	int sum =0;

	/*llenando la matriz*/
	#pragma omp parallel for schedule (static) private (i, j) 
	for(i=0; i<tam;i++){
			vec_master[i] =0;
		for(j=0; j<tam;j++){
			mat[i][j] = 1;
			
		}
	}

	#pragma omp parallel for reduction(+: sum)
	for(i=0; i<tam;i++){
		for(j=0; j<tam;j++){
			sum += mat[i][j];
		}
	}
	
	cout <<"************************************"<<endl;
	cout <<"variable sum : "<<sum<<"           *"<<endl;
	cout <<"************************************"<<endl;

	cout <<"************************************"<<endl;
	cout <<"           Matriz contenedora      *"<<endl;
	cout <<"************************************"<<endl;

	for(i=0; i<tam;i++){
		for(j=0; j<tam;j++){
			cout << mat[i][j]<<" " ;
		}
		cout<<endl;
	}


	/*sumar el contenedor sum a cada poscicion del vector MPI*/

	int proc,rank;
	int tag = 10;
	int root = 0;
	
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &proc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int rest = (tam%(proc-1));
    print(vec_master);
    if (rank == 0)
    {
    	sumar(sum,vec_master);
    	int i,j;
    	for(i =0; i< proc;++i)
    	{
    		MPI_Send(&vec_master[i],sum,MPI_INT,i,tag,MPI_COMM_WORLD);
            i+=sum;
       	}
    }
    if (rest >0)
    {
    	int i;
    	for ( i = 0; i < proc; ++i)
    	{
    		vec_master[i] += sum;
    	}
    }
    {
    	MPI_Recv(&vec_master[i],1,MPI_INT,0,tag,MPI_COMM_WORLD,&status);
    }
 
   MPI_Finalize();
    
    
	sumar(sum,vec_master);
    cout <<"************************************"<<endl;
	cout <<"           Vector Maestro      	   * "<<endl;
	cout <<"************************************"<<endl;
    print(vec_master);	

	return 0;
}